import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'jackphotogallery';
  oldScrollValue = 0;
  showFab = false;
  categories = [
    "All",
    "Jack",
    "Flowers",
    "Landscape",
    "Other"
  ]
  ngOnInit(): void {
    window.addEventListener("scroll", (e) => {
      // Get the new Value
      let newValue = window.pageYOffset;

      //Subtract the two and conclude
      if(this.oldScrollValue - newValue < 0){
          this.showFab = false;
      } else if(this.oldScrollValue - newValue > 0){
        if(window.scrollY < 150) {
          this.showFab = false;
          return;
        }
        this.showFab = true;
      }
      // Update the old value
      this.oldScrollValue = newValue;
    })
  }

  filter() {
    let text = document.getElementById("filter").innerHTML
    this.categories.forEach((cat, index) => {
      if (cat === text) {
        document.getElementById("filter").innerHTML = this.categories[(index + 1) % this.categories.length];
        this.showCategory(document.getElementById("filter").innerHTML)
      }
    });
    
  }
    showCategory(cat: string) {
      Array.from(document.getElementsByClassName("homeImg")).forEach(element => {
        element.classList.add("hide")
      });
      cat === "Other" ? Array.from(document.getElementsByClassName("Other")).forEach(element => {
        element.classList.remove("hide")
      }) : "";
      cat === "Flowers" ? Array.from(document.getElementsByClassName("Flowers")).forEach(element => {
        element.classList.remove("hide")
      }) : "";
      cat === "Jack" ? Array.from(document.getElementsByClassName("Jack")).forEach(element => {
        element.classList.remove("hide")
      }) : "";
      cat === "Landscape" ? Array.from(document.getElementsByClassName("Landscape")).forEach(element => {
        element.classList.remove("hide")
      }) : "";
      cat === "All" ? Array.from(document.getElementsByClassName("homeImg")).forEach(element => {
        element.classList.remove("hide")
      }) : "";
    }

    goToSection(sec: number) {
      switch (sec) {
        case 0:
          window.scrollTo({left: 0, top: 0, behavior: 'smooth'});
          break;
        case 1:
          let section = document.getElementById("beforeAfterSection")
          window.scrollTo({left: 0, top: section.offsetTop, behavior: 'smooth'})
          break;
        default:
          window.scrollTo({left: 0, top: 0, behavior: 'smooth'})
          break;
      }
      
    }
}
